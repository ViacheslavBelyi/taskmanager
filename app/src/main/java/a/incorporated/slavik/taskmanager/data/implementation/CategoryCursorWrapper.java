package a.incorporated.slavik.taskmanager.data.implementation;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;

import a.incorporated.slavik.taskmanager.data.logic.TaskDatabaseSchema;
import a.incorporated.slavik.taskmanager.domain.entity.Category;

public class CategoryCursorWrapper extends CursorWrapper {

    private final Cursor cursor;

    public static CategoryCursorWrapper getInstance(SQLiteDatabase database, String tableName){
        Cursor cursor = database.query(tableName, null, null, null, null, null, null);
        return new CategoryCursorWrapper(cursor);
    }

    private CategoryCursorWrapper(Cursor cursor) {
        super(cursor);
        this.cursor = cursor;

        cursor.moveToFirst();
    }


    public Category getNextCategory() {
        Category category = null;

        if (  !cursor.isAfterLast() ) {
            int key = getKey();
            String name = getName();
            int color = getColor();

            category = new Category(key, name, color);

            cursor.moveToNext();
        }

        return category;
    }

    private int getKey(){
        return cursor.getInt(cursor.getColumnIndex(TaskDatabaseSchema.CategoryTable.Columns.KEY));
    }

    private String getName(){
        return cursor.getString(cursor.getColumnIndex(TaskDatabaseSchema.CategoryTable.Columns.NAME));
    }

    private int getColor(){
        return cursor.getInt(cursor.getColumnIndex(TaskDatabaseSchema.CategoryTable.Columns.COLOR));
    }

}
