package a.incorporated.slavik.taskmanager.data.implementation;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import a.incorporated.slavik.taskmanager.data.logic.TaskDatabaseSchema;

public class OpenHelper extends SQLiteOpenHelper {
    private final static int VERSION = 1;
    private final static String DB_NAME = "task.db";

    public OpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(" create table " + TaskDatabaseSchema.CategoryTable.TABLE_NAME + "( " +
                TaskDatabaseSchema.CategoryTable.Columns.KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TaskDatabaseSchema.CategoryTable.Columns.NAME + ", " +
                TaskDatabaseSchema.CategoryTable.Columns.COLOR + " INTEGER " +
                ")");

        db.execSQL(" create table " + TaskDatabaseSchema.TaskTable.TABLE_NAME + "( " +
                TaskDatabaseSchema.TaskTable.Columns.KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TaskDatabaseSchema.TaskTable.Columns.NAME + ", " +
                TaskDatabaseSchema.TaskTable.Columns.DATE + ", " +

                TaskDatabaseSchema.TaskTable.Columns.CATEGORY_KEY + " INTEGER NOT NULL, " +
                    " FOREIGN KEY(" + TaskDatabaseSchema.TaskTable.Columns.CATEGORY_KEY + ")" +
                    " REFERENCES " + TaskDatabaseSchema.CategoryTable.TABLE_NAME + "(" +
                        TaskDatabaseSchema.CategoryTable.Columns.KEY + ")" +
         ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
