package a.incorporated.slavik.taskmanager.data.implementation;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;

import a.incorporated.slavik.taskmanager.data.logic.TaskDatabaseSchema;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Task;

public class TaskCursorWrapper extends CursorWrapper {

    private final Cursor cursor;

    public static TaskCursorWrapper getInstance(SQLiteDatabase database, String tableName){
        Cursor cursor = database.query(tableName, null, null, null, null, null, null);
        return new TaskCursorWrapper(cursor);
    }

    private TaskCursorWrapper(Cursor cursor) {
        super(cursor);
        this.cursor = cursor;

        cursor.moveToFirst();
    }

    public int getKey(){
        return cursor.getInt(cursor.getColumnIndex(TaskDatabaseSchema.TaskTable.Columns.KEY));
    }

    public String getName(){
        return cursor.getString(cursor.getColumnIndex(TaskDatabaseSchema.TaskTable.Columns.NAME));
    }

    public String getDate(){
        return cursor.getString(cursor.getColumnIndex(TaskDatabaseSchema.TaskTable.Columns.DATE));
    }

    public int getCategoryKey(){
        return cursor.getInt(cursor.getColumnIndex(TaskDatabaseSchema.TaskTable.Columns.CATEGORY_KEY));
    }


}
