package a.incorporated.slavik.taskmanager.data.implementation.async;

import android.os.AsyncTask;

import a.incorporated.slavik.taskmanager.data.implementation.async.strategy.Strategy;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.domain.entity.Category;

public class CategoryLoader extends AsyncTask<Category, Void, Void> {

    private final CategoryRepository repository;
    private final Strategy strategy;

    public CategoryLoader(Strategy strategy, CategoryRepository repository) {
        this.strategy = strategy;
        this.repository = repository;
    }


    @Override
    protected Void doInBackground(Category... categories) {
        for (Category category : categories) {
            strategy.actWithRepository(repository, category);
        }
        return null;
    }

}

