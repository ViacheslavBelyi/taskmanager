package a.incorporated.slavik.taskmanager.data.implementation.async;

import android.os.AsyncTask;

import java.util.List;

import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;

public class CategoryReader extends AsyncTask<Void, Void, List<Category>> {

    private final CategoryRepository repository;

    public CategoryReader(CategoryRepository repository){
        this.repository = repository;
    }

    @Override
    protected List<Category> doInBackground(Void... voids) {
        return repository.getAll();
    }
}
