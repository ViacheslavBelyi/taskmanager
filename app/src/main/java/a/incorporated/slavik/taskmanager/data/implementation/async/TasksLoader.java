package a.incorporated.slavik.taskmanager.data.implementation.async;

import android.os.AsyncTask;

import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.domain.entity.Task;
import a.incorporated.slavik.taskmanager.data.implementation.async.strategy.Strategy;

public class TasksLoader extends AsyncTask<Task, Void, Void> {

    private final TaskRepository repository;
    private final Strategy strategy;

    public TasksLoader(Strategy strategy, TaskRepository repository) {
        this.strategy = strategy;
        this.repository = repository;
    }


    @Override
    protected Void doInBackground(Task... tasks) {
        for (Task task : tasks) {
            strategy.actWithRepository(repository, task);
        }
        return null;
    }


}



