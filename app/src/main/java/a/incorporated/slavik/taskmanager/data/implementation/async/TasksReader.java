package a.incorporated.slavik.taskmanager.data.implementation.async;

import android.os.AsyncTask;

import java.util.List;

import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.domain.entity.Task;

public class TasksReader extends AsyncTask<Void, Void, List<Task>> {

    private final TaskRepository repository;

    public TasksReader(TaskRepository repository){
        this.repository = repository;
    }

    @Override
    protected List<Task> doInBackground(Void... voids) {
        return repository.getAll();
    }
}