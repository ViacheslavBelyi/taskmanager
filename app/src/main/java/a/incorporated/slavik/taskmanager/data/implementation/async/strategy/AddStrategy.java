package a.incorporated.slavik.taskmanager.data.implementation.async.strategy;

import a.incorporated.slavik.taskmanager.data.logic.dao.Repository;
import a.incorporated.slavik.taskmanager.domain.entity.Item;

public class AddStrategy implements Strategy {
    @Override
    public void actWithRepository(Repository repository, Item item) {
        repository.add(item);
    }
}
