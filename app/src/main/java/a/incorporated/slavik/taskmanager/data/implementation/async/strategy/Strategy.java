package a.incorporated.slavik.taskmanager.data.implementation.async.strategy;

import a.incorporated.slavik.taskmanager.data.logic.dao.Repository;
import a.incorporated.slavik.taskmanager.domain.entity.Item;

public interface Strategy {
    void actWithRepository(Repository repository, Item item);
}