package a.incorporated.slavik.taskmanager.data.implementation.async.strategy;

import a.incorporated.slavik.taskmanager.data.logic.dao.Repository;
import a.incorporated.slavik.taskmanager.domain.entity.Item;

public class UpdateStrategy implements Strategy {
    @Override
    public void actWithRepository(Repository repository, Item item) {
        repository.update(item);
    }
}
