package a.incorporated.slavik.taskmanager.data.implementation.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import a.incorporated.slavik.taskmanager.data.implementation.CategoryCursorWrapper;
import a.incorporated.slavik.taskmanager.data.implementation.GetAllSpecification;
import a.incorporated.slavik.taskmanager.data.logic.Specification;
import a.incorporated.slavik.taskmanager.data.logic.TaskDatabaseSchema;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.data.logic.dao.Repository;
import a.incorporated.slavik.taskmanager.domain.entity.Category;

public class CategoryDaoSQL implements CategoryRepository {

    private final SQLiteDatabase db;

    public CategoryDaoSQL(SQLiteDatabase database){
        this.db = database;
    }

    @Override
    public void add(Category category) {
        ContentValues values = new ContentValues();

        values.put(TaskDatabaseSchema.CategoryTable.Columns.NAME, category.getName());
        values.put(TaskDatabaseSchema.CategoryTable.Columns.COLOR, category.getColor());

        db.insert(TaskDatabaseSchema.CategoryTable.TABLE_NAME, null, values);
    }

    @Override
    public void update(Category category) {

        db.execSQL(" update " + TaskDatabaseSchema.CategoryTable.TABLE_NAME + " set " +
                TaskDatabaseSchema.CategoryTable.Columns.NAME + " = \"" + category.getName() + "\" , " +
                TaskDatabaseSchema.CategoryTable.Columns.COLOR + " = " + category.getColor() + " " +
                "where " + TaskDatabaseSchema.CategoryTable.Columns.KEY + " = " + category.getKey() );

    }

    @Override
    public void delete(Category category) {
        db.execSQL(" DELETE FROM " + TaskDatabaseSchema.CategoryTable.TABLE_NAME + " " +
                " where " + TaskDatabaseSchema.CategoryTable.Columns.KEY + " = " + category.getKey() );
    }

    @Override
    public List<Category> getAll() {
        return query(new GetAllSpecification());
    }


    @Override
    public List<Category> query(Specification specification) {

        List<Category> categories = new ArrayList<>();
        CategoryCursorWrapper cursor = CategoryCursorWrapper.getInstance(db, TaskDatabaseSchema.CategoryTable.TABLE_NAME);

        while ( ! cursor.isAfterLast()) {
            Category category = cursor.getNextCategory();
            if (specification.specified(category)) {
                categories.add(category);
            }
        }
        cursor.close();

        return categories;
    }

}
