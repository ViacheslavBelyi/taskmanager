package a.incorporated.slavik.taskmanager.data.implementation.dao;

import java.util.List;
import java.util.concurrent.ExecutionException;

import a.incorporated.slavik.taskmanager.data.implementation.async.CategoryLoader;
import a.incorporated.slavik.taskmanager.data.implementation.async.CategoryReader;
import a.incorporated.slavik.taskmanager.data.implementation.async.strategy.AddStrategy;
import a.incorporated.slavik.taskmanager.data.implementation.async.strategy.DeleteStrategy;
import a.incorporated.slavik.taskmanager.data.implementation.async.strategy.UpdateStrategy;
import a.incorporated.slavik.taskmanager.data.logic.Specification;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.domain.entity.Category;


public class CategoryRepositoryImpl implements CategoryRepository {

    private final CategoryRepository dao;

    public CategoryRepositoryImpl(CategoryRepository categoryDao){
        this.dao = categoryDao;
    }

    @Override
    public void add(Category item) {
        //dao.add(item);
        new CategoryLoader(new AddStrategy(), dao).execute(item);
    }

    @Override
    public void update(Category item) {
        //dao.update(item);
        new CategoryLoader(new UpdateStrategy(), dao).execute(item);
    }

    @Override
    public void delete(Category item) {
        //dao.delete(item);
        new CategoryLoader(new DeleteStrategy(), dao).execute(item);
    }

    @Override
    public List<Category> getAll() {
        //return query(new GetAllSpecification());
        CategoryReader reader = new CategoryReader(dao);

        List<Category> categories = null;
        try {
            categories = reader.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return categories;
    }

    @Override
    public List<Category> query(Specification specification) {
        return dao.query(specification);
    }
}
