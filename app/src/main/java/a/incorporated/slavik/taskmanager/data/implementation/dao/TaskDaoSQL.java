package a.incorporated.slavik.taskmanager.data.implementation.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import a.incorporated.slavik.taskmanager.data.implementation.CategoryCursorWrapper;
import a.incorporated.slavik.taskmanager.data.implementation.GetAllSpecification;
import a.incorporated.slavik.taskmanager.data.implementation.TaskCursorWrapper;
import a.incorporated.slavik.taskmanager.data.logic.Specification;
import a.incorporated.slavik.taskmanager.data.logic.TaskDatabaseSchema;
import a.incorporated.slavik.taskmanager.data.logic.dao.Repository;
import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Task;

public class TaskDaoSQL implements TaskRepository {

    private final SQLiteDatabase db;

    public TaskDaoSQL(SQLiteDatabase database){
        this.db = database;
    }


    @Override
    public void add(Task task) {

        ContentValues values = new ContentValues();

        values.put(TaskDatabaseSchema.TaskTable.Columns.NAME, task.getName());
        values.put(TaskDatabaseSchema.TaskTable.Columns.DATE, task.getDate() );
        values.put(TaskDatabaseSchema.TaskTable.Columns.CATEGORY_KEY, task.getCategory().getKey() );

        db.insert(TaskDatabaseSchema.TaskTable.TABLE_NAME, null, values);
    }

    @Override
    public void update(Task task) {

        String dateString = task.getDate();

        db.execSQL(" update " + TaskDatabaseSchema.TaskTable.TABLE_NAME + " set " +
                TaskDatabaseSchema.TaskTable.Columns.NAME + " = \"" + task.getName() + "\" , " +
                TaskDatabaseSchema.TaskTable.Columns.DATE + " = \"" + dateString + "\" , " +
                TaskDatabaseSchema.TaskTable.Columns.CATEGORY_KEY + " = " + task.getCategory().getKey() + " " +
                "where " + TaskDatabaseSchema.TaskTable.Columns.KEY + " = " + task.getKey() );
    }

    @Override
    public void delete(Task task) {
        db.execSQL(" DELETE FROM " + TaskDatabaseSchema.TaskTable.TABLE_NAME + " " +
                " where " + TaskDatabaseSchema.TaskTable.Columns.KEY + " = " + task.getKey() );
    }

    @Override
    public List<Task> getAll() {
        return query(new GetAllSpecification());
    }

    @Override
    public List<Task> query(Specification specification) {

        List<Task> tasks = new ArrayList<>();
        TaskCursorWrapper cursor = TaskCursorWrapper.getInstance(db, TaskDatabaseSchema.TaskTable.TABLE_NAME);

        while ( ! cursor.isAfterLast()) {

            int key = cursor.getKey();
            String name = cursor.getName();
            String date = cursor.getDate();
            Category category = getCategory(cursor.getCategoryKey());

            Task task = new Task(key, name, date, category);

            if (specification.specified(task)) {
                tasks.add(task);
            }
            cursor.moveToNext();
        }
        cursor.close();

        return tasks;
    }


    private Category getCategory(int key){
        //todo make table name unique in case simultaneous access to this
        final String TEMPORARY_TABLE_NAME = "a_incorporated_slavik_CategoryDaoSql_temporary_table_name";

        db.execSQL(" drop table if exists " + TEMPORARY_TABLE_NAME);

        db.execSQL(" create table " + TEMPORARY_TABLE_NAME + "(" +
                TaskDatabaseSchema.CategoryTable.Columns.KEY + ", " +
                TaskDatabaseSchema.CategoryTable.Columns.NAME + ", " +
                TaskDatabaseSchema.CategoryTable.Columns.COLOR + " " +

                ")");

        db.execSQL("insert into " + TEMPORARY_TABLE_NAME +
                " SELECT * FROM " + TaskDatabaseSchema.CategoryTable.TABLE_NAME +
                " WHERE " + TaskDatabaseSchema.CategoryTable.Columns.KEY + " = " + key
        );

        CategoryCursorWrapper cursor = CategoryCursorWrapper.getInstance(db, TEMPORARY_TABLE_NAME);
        Category category = cursor.getNextCategory();
        cursor.close();

        return category;
    }

}
