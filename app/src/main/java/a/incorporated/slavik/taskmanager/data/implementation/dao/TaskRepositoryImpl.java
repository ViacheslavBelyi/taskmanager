package a.incorporated.slavik.taskmanager.data.implementation.dao;

import java.util.List;
import java.util.concurrent.ExecutionException;

import a.incorporated.slavik.taskmanager.data.implementation.async.TasksLoader;
import a.incorporated.slavik.taskmanager.data.implementation.async.TasksReader;
import a.incorporated.slavik.taskmanager.data.implementation.async.strategy.AddStrategy;
import a.incorporated.slavik.taskmanager.data.implementation.async.strategy.DeleteStrategy;
import a.incorporated.slavik.taskmanager.data.implementation.async.strategy.UpdateStrategy;
import a.incorporated.slavik.taskmanager.data.logic.Specification;
import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.domain.entity.Task;

public class TaskRepositoryImpl implements TaskRepository {

    private final TaskRepository dao;

    public TaskRepositoryImpl(TaskRepository taskDao){
        this.dao = taskDao;
    }

    @Override
    public void add(Task item) {
        //dao.add(item);
        new TasksLoader(new AddStrategy(), dao).execute(item);
    }

    @Override
    public void update(Task item) {
        dao.update(item);
        new TasksLoader(new UpdateStrategy(), dao).execute(item);
    }

    @Override
    public void delete(Task item) {
        dao.delete(item);
        new TasksLoader(new DeleteStrategy(), dao).execute(item);
    }

    @Override
    public List<Task> getAll() {
        //return query(new GetAllSpecification());

        TasksReader reader = new TasksReader(dao);

        List<Task> tasks = null;
        try {
            tasks = reader.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return tasks;
    }

    @Override
    public List<Task> query(Specification specification) {
        return dao.query(specification);                // ! not async
    }
}
