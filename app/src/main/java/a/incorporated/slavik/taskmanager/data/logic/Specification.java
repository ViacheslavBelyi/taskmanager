package a.incorporated.slavik.taskmanager.data.logic;

import a.incorporated.slavik.taskmanager.domain.entity.Item;

public interface Specification<ItemImpl extends Item> {
    boolean specified(ItemImpl t);
}
