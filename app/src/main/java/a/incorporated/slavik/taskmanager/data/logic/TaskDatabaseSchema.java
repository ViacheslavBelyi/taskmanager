package a.incorporated.slavik.taskmanager.data.logic;

public class TaskDatabaseSchema{

    public class TaskTable {
        public final static String TABLE_NAME = "Task";

        public class Columns {
            public final static String KEY = "_id";
            public final static String NAME = "name";
            public final static String DATE = "date";
            public final static String CATEGORY_KEY = CategoryTable.Columns.KEY;
        }
    }

    public class CategoryTable {
        public final static String TABLE_NAME = "Category";

        public class Columns {
            public final static String KEY = "_idCategory";
            public final static String NAME = "name";
            public final static String COLOR = "color";
        }

    }
}
