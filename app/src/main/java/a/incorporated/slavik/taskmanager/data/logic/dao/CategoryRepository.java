package a.incorporated.slavik.taskmanager.data.logic.dao;

import a.incorporated.slavik.taskmanager.domain.entity.Category;

public interface CategoryRepository extends Repository<Category> {
}
