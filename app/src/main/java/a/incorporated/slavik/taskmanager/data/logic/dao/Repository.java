package a.incorporated.slavik.taskmanager.data.logic.dao;

import java.util.List;

import a.incorporated.slavik.taskmanager.data.logic.Specification;
import a.incorporated.slavik.taskmanager.domain.entity.Item;

public interface Repository <ItemImpl extends Item> {
    void add(ItemImpl item);
    void update(ItemImpl item);
    void delete(ItemImpl item);

    List<ItemImpl> getAll();

    List<ItemImpl> query(Specification specification);
}
