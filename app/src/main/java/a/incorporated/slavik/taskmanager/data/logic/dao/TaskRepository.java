package a.incorporated.slavik.taskmanager.data.logic.dao;

import a.incorporated.slavik.taskmanager.domain.entity.Task;

public interface TaskRepository extends Repository<Task> {
}
