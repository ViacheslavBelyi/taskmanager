package a.incorporated.slavik.taskmanager.di.app;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;

public interface AppDiProvider {
    SQLiteOpenHelper provideOpenHelper();
    SQLiteDatabase provideDatabase();
    TaskRepository provideTaskRepository();
    CategoryRepository provideCategoryRepository();
}
