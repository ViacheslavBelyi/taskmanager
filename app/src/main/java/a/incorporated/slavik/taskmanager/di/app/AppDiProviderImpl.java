package a.incorporated.slavik.taskmanager.di.app;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import a.incorporated.slavik.taskmanager.data.implementation.OpenHelper;
import a.incorporated.slavik.taskmanager.data.implementation.dao.CategoryDaoSQL;
import a.incorporated.slavik.taskmanager.data.implementation.dao.CategoryRepositoryImpl;
import a.incorporated.slavik.taskmanager.data.implementation.dao.TaskDaoSQL;
import a.incorporated.slavik.taskmanager.data.implementation.dao.TaskRepositoryImpl;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.data.logic.dao.Repository;
import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Task;

public class AppDiProviderImpl extends Application implements AppDiProvider {

    private OpenHelper openHelper;

    @Override
    public SQLiteDatabase provideDatabase() {
        return provideOpenHelper().getWritableDatabase();
    }

    @Override
    public SQLiteOpenHelper provideOpenHelper() {
        if (openHelper == null){
            openHelper = new OpenHelper(this);
        }
        return openHelper;
    }

    @Override
    public CategoryRepository provideCategoryRepository() {
        CategoryRepository dao = new CategoryDaoSQL(provideDatabase());
        return new CategoryRepositoryImpl(dao);
    }

    @Override
    public TaskRepository provideTaskRepository() {
        TaskRepository dao = new TaskDaoSQL(provideDatabase());
        return new TaskRepositoryImpl(dao);
    }

}
