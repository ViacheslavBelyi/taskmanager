package a.incorporated.slavik.taskmanager.di.fragment;

import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;

public interface CategoriesFragmentDiProvider {
    CategoryRepository provideRepository();
}
