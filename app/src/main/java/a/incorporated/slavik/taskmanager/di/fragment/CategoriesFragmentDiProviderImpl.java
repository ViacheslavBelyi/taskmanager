package a.incorporated.slavik.taskmanager.di.fragment;

import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;

public class CategoriesFragmentDiProviderImpl implements CategoriesFragmentDiProvider {

    private final AppDiProvider provider;

    public CategoriesFragmentDiProviderImpl(AppDiProvider provider){
        this.provider = provider;
    }


    @Override
    public CategoryRepository provideRepository() {
        return provider.provideCategoryRepository();
    }

}
