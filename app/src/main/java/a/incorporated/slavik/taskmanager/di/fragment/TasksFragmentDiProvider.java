package a.incorporated.slavik.taskmanager.di.fragment;

import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;

public interface TasksFragmentDiProvider {
    TaskRepository provideRepository();
}
