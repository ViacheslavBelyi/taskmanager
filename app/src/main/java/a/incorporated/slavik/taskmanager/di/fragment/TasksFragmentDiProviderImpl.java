package a.incorporated.slavik.taskmanager.di.fragment;

import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;

public class TasksFragmentDiProviderImpl implements TasksFragmentDiProvider {

    private final AppDiProvider provider;

    public TasksFragmentDiProviderImpl(AppDiProvider provider){
        this.provider = provider;
    }

    @Override
    public TaskRepository provideRepository() {
        return provider.provideTaskRepository();
    }





}
