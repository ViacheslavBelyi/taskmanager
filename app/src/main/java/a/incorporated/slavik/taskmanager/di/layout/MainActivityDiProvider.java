package a.incorporated.slavik.taskmanager.di.layout;

import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;
import a.incorporated.slavik.taskmanager.domain.contract.main_screen.MainContract;
import a.incorporated.slavik.taskmanager.domain.router.MainRouter;
import a.incorporated.slavik.taskmanager.ui.main_screen.fragment.CategoriesFragment;
import a.incorporated.slavik.taskmanager.ui.main_screen.fragment.TasksFragment;

public interface MainActivityDiProvider extends AppDiProvider {
    MainRouter provideRouter();
    MainContract.Presenter providePresenter();

    CategoriesFragment provideCategoryFragment();
    TasksFragment provideTasksFragment();
}
