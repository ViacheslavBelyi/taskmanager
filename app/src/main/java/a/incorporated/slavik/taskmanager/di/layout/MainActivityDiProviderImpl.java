package a.incorporated.slavik.taskmanager.di.layout;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import a.incorporated.slavik.taskmanager.ui.main_screen.MainActivity;
import a.incorporated.slavik.taskmanager.ui.main_screen.MainPresenter;
import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;
import a.incorporated.slavik.taskmanager.domain.contract.main_screen.MainContract;
import a.incorporated.slavik.taskmanager.domain.router.MainRouter;
import a.incorporated.slavik.taskmanager.ui.main_screen.router.MainRouterImpl;
import a.incorporated.slavik.taskmanager.ui.main_screen.fragment.CategoriesFragment;
import a.incorporated.slavik.taskmanager.ui.main_screen.fragment.TasksFragment;

public class MainActivityDiProviderImpl implements MainActivityDiProvider {

    private final MainActivity mainActivity;
    private final AppDiProvider appDiProvider;

    public MainActivityDiProviderImpl(MainActivity mainActivity, AppDiProvider appProvider) {
        this.mainActivity = mainActivity;
        this.appDiProvider = appProvider;
    }

    @Override
    public MainRouter provideRouter() {
        return new MainRouterImpl(mainActivity);
    }

    @Override
    public MainContract.Presenter providePresenter() {
        return new MainPresenter(mainActivity,
                R.id.task_menu_item, R.id.category_menu_item, R.id.settings_menu_item);
    }

    @Override
    public CategoriesFragment provideCategoryFragment() {
        return new CategoriesFragment();
    }

    @Override
    public TasksFragment provideTasksFragment() {
        return new TasksFragment();
    }

    @Override
    public SQLiteOpenHelper provideOpenHelper() {
        return appDiProvider.provideOpenHelper();
    }

    @Override
    public SQLiteDatabase provideDatabase() {
        return appDiProvider.provideDatabase();
    }

    @Override
    public TaskRepository provideTaskRepository() {
        return appDiProvider.provideTaskRepository();
    }

    @Override
    public CategoryRepository provideCategoryRepository() {
        return appDiProvider.provideCategoryRepository();
    }
}
