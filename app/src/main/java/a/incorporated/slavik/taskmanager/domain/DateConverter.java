package a.incorporated.slavik.taskmanager.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class DateConverter {

    private static final String PATTERN = "yyyy-MM-dd";
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat(PATTERN);

    public static String convertDate(long date){
        return FORMAT.format(date);
    }

    public static Date convertDate(String dateString){
        Date date = null;

        try {
            date = FORMAT.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


}
