package a.incorporated.slavik.taskmanager.domain.contract.edit_screen;

import a.incorporated.slavik.taskmanager.domain.entity.Category;

public interface AddCategoryContract {

    interface View {

        void finishView();

        void saveCategory();

        void setCategoryName(String name);
        void setCategoryColor(int color);

        int[] getColors();

        Category getCategory();

        void onEmptyNameError();
    }

    interface Presenter {

        void onAddButtonClicked();
        void onCloseButtonClicked();

        void onNameChange(String name);
        void onColorChange(int selectedColorPosition);

    }

}
