package a.incorporated.slavik.taskmanager.domain.contract.edit_screen;

import java.util.List;

import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Task;

public interface AddTaskContract {

    interface View {

        void finishView();

        void saveTask();

        void setNameField(String nameField);
        void setDateField(long date);
        void setSelectedCategory(int position);

        void setTaskName(String name);
        void setTaskDate(String date);
        void setTaskCategory(Category category);

        String getSelectedCategoryName();
        List<Category> getCategories();

        Task getTask();

        void onEmptyNameError();
    }

    interface Presenter {
        void onCreateView();

        void onAddButtonClicked();
        void onCloseButtonClicked();

        void onNameChange(String name);
        void onDateChange(int year, int month, int day);
    }
}
