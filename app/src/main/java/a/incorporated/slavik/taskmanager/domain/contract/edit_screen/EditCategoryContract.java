package a.incorporated.slavik.taskmanager.domain.contract.edit_screen;

import java.util.List;

import a.incorporated.slavik.taskmanager.domain.entity.Task;

public interface EditCategoryContract {

    interface View extends AddCategoryContract.View {

        void setNameField(String nameField);
        void setSelectedColorField(int position);
        String getSelectedColorName();

        void updateCategory();
        void deleteCategory();

        List<Task> getTasks();
        void onRemoveError();
    }

    interface Presenter extends AddCategoryContract.Presenter {

        void onCreateView();

        void onUpdateButtonClicked();
        void onDeleteButtonClicked();
    }

}
