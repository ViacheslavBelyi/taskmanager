package a.incorporated.slavik.taskmanager.domain.contract.edit_screen;

public interface EditTaskContract {

    interface View extends AddTaskContract.View{

        void updateTask();
        void deleteTask();

    }

    interface Presenter extends AddTaskContract.Presenter {

        void onUpdateButtonClicked();
        void onDeleteButtonClicked();

        void onCategorySelected(int position);

    }
}
