package a.incorporated.slavik.taskmanager.domain.contract.main_screen;

import a.incorporated.slavik.taskmanager.domain.entity.Item;

public interface ItemFragmentContract {

    interface View {
        void startAddActivity();
        void startEditActivity(Item item);
        ItemFragmentContract.Presenter getPresenter();
    }

    interface ItemsView {

    }

    interface Presenter {
        void onAddButtonClicked();
        void onItemTapped(Item item);
    }

}

