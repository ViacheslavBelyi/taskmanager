package a.incorporated.slavik.taskmanager.domain.contract.main_screen;

public interface MainContract {

    interface View{
        void showTasks();
        void showCategories();
        void showSettings();
    }

    interface Presenter{
        void onMenuItemSelected(int itemId);
    }

}
