package a.incorporated.slavik.taskmanager.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Category implements Item, Parcelable {

    private int key;
    private String name;
    private int color;

    public Category(){
    }

    public Category(int key, String name, int color){
        this.key = key;
        this.name = name;
        this.color = color;
    }


    public void setKey(int key) {
        this.key = key;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(int color) {
        this.color = color;
    }


    public String getName() {
        return name;
    }

    public int getColor() {
        return color;
    }

    public int getKey() {
        return key;
    }

    public boolean equals(Category category) {
        return key == category.getKey();
    }


    @Override
    public String toString() {
        return name + " " + color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(key);
        dest.writeString(name);
        dest.writeInt(color);
    }


    Category(Parcel in) {
        key = in.readInt();
        name = in.readString();
        color = in.readInt();
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

}