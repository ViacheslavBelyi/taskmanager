package a.incorporated.slavik.taskmanager.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Task implements Item, Parcelable {

    private int key;
    private String name;
    private String date;
    private Category category;

    public Task(){
        category = new Category();
    }

    public Task(int key, String name, String date, Category category) {
        this.key = key;
        this.name = name;
        this.date = date;
        this.category = category;
    }



    public void setKey(int key) {
        this.key = key;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


    public int getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public Category getCategory() {
        return category;
    }


    @Override
    public String toString() {
        return name + " " + date + " " + category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(key);
        dest.writeString(name);
        dest.writeString(date);

        dest.writeParcelable(category, flags);
    }

    private Task(Parcel in) {
        key = in.readInt();
        name = in.readString();
        date = in.readString();

        category = in.readParcelable(Category.class.getClassLoader());
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };
}
