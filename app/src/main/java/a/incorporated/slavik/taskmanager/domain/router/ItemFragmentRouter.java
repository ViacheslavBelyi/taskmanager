package a.incorporated.slavik.taskmanager.domain.router;

import a.incorporated.slavik.taskmanager.domain.entity.Item;

public interface ItemFragmentRouter {
    void startAddItemActivity();
    void startEditItemActivity(Item item);
}
