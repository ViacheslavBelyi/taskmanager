package a.incorporated.slavik.taskmanager.domain.router;

public interface MainRouter {
    void showTasks();
    void showCategories();
    void showSettings();
}
