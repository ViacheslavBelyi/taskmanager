package a.incorporated.slavik.taskmanager.domain.router;

public interface TaskFragmentRouter {
    void startAddTaskActivity();
    void startEditTaskActivity();
}
