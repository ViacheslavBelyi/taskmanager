package a.incorporated.slavik.taskmanager.ui.edit_screen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.HashMap;
import java.util.Map;

import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;
import a.incorporated.slavik.taskmanager.domain.contract.edit_screen.AddCategoryContract;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AddCategoryActivity extends AppCompatActivity implements AddCategoryContract.View {

    public static final String[] COLORS_NAMES = {"red", "orange", "yellow", "green", "blue", "violet"};
    public static final int[] COLORS = {R.color.red, R.color.orange, R.color.yellow, R.color.green, R.color.blue, R.color.violet};

    @BindView(R.id.close_button)
    ImageButton closeButton;
    @BindView(R.id.add_item_fab)
    FloatingActionButton addButton;

    @BindView(R.id.nameEditText)
    EditText nameEditText;
    @BindView(R.id.categorySpinner)
    Spinner spinner;

    private Category category;

    private AddCategoryContract.Presenter presenter;

    private CategoryRepository repository;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_category_activity);

        ButterKnife.bind(this);

        closeButton.setOnClickListener(new OnClickCloseButtonListener());
        addButton.setOnClickListener(new OnClickAddButtonListener());

        nameEditText.addTextChangedListener(new NameTextWatcher());
        spinner.setOnItemSelectedListener(new OnItemSelectedListener());


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.categories_colors, R.layout.spinner);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(adapter);


        category = new Category();

        presenter = new AddCategoryPresenter(this);

        AppDiProvider provider = ((AppDiProvider) getApplication());

        repository = provider.provideCategoryRepository();

    }

    @Override
    public void finishView() {
        finish();
    }

    @Override
    public void saveCategory() {
        repository.add(category);
    }

    @Override
    public void setCategoryName(String name) {
        category.setName(name);
    }

    @Override
    public void setCategoryColor(int color) {
        category.setColor(color);
    }

    @Override
    public int[] getColors() {
        return AddCategoryActivity.COLORS;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public void onEmptyNameError() {
        new EmptyNameDialogFragment().show(getSupportFragmentManager(), "EmptyName");  // todo needless tag?
    }



    public static class EmptyNameDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final String TITLE = "Can`t save";
            final String MESSAGE = "Fill name field!";

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialog);
            builder.setTitle(TITLE);
            builder.setMessage(MESSAGE);
            builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });

            return builder.create();
        }
    }


    private class OnClickCloseButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            presenter.onCloseButtonClicked();
        }
    }

    private class OnClickAddButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            presenter.onAddButtonClicked();
        }

    }

    private class NameTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            presenter.onNameChange(s.toString());
        }
    }

    private class OnItemSelectedListener implements Spinner.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            presenter.onColorChange(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

}
