package a.incorporated.slavik.taskmanager.ui.edit_screen;

import a.incorporated.slavik.taskmanager.domain.contract.edit_screen.AddCategoryContract;

public class AddCategoryPresenter implements AddCategoryContract.Presenter {

    private final AddCategoryContract.View view;

    AddCategoryPresenter(AddCategoryContract.View view){
        this.view = view;
    }

    @Override
    public void onAddButtonClicked() {
        boolean canSaveCategory = true;

        String categoryName = view.getCategory().getName();

        if (categoryName == null || categoryName.length() == 0){
            view.onEmptyNameError();
            canSaveCategory = false;
        }


        if (canSaveCategory){
            view.saveCategory();
            view.finishView();
        }

    }

    @Override
    public void onCloseButtonClicked() {
        view.finishView();
    }

    @Override
    public void onNameChange(String name) {
        view.setCategoryName(name);
    }

    @Override
    public void onColorChange(int selectedColorPosition) {
        int[] colors = view.getColors();
        view.setCategoryColor(colors[selectedColorPosition]);
    }




}
