package a.incorporated.slavik.taskmanager.ui.edit_screen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;
import a.incorporated.slavik.taskmanager.domain.contract.edit_screen.AddTaskContract;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Task;
import a.incorporated.slavik.taskmanager.data.implementation.async.TasksLoader;
import a.incorporated.slavik.taskmanager.data.implementation.async.strategy.AddStrategy;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AddTaskActivity extends AppCompatActivity implements AddTaskContract.View {

    private static final String TASK_TAG = "TASK_KEY";

    @BindView(R.id.nameEditText)
    EditText nameEditText;
    @BindView(R.id.calendarView)
    CalendarView calendarView;
    @BindView(R.id.categoriesSpinner)
    Spinner categoriesSpinner;

    @BindView(R.id.close_button)
    ImageButton closeButton;
    @BindView(R.id.add_item_fab)
    FloatingActionButton addButton;

    private Task task;

    private AddTaskContract.Presenter presenter;

    private List<Category> categories;

    private TaskRepository taskRepository;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_task_activity);

        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            task = new Task();
        } else {
            task = savedInstanceState.getParcelable(TASK_TAG);
        }

        closeButton.setOnClickListener(new OnClickCloseButtonListener());
        addButton.setOnClickListener(new OnClickAddButtonListener());

        nameEditText.addTextChangedListener(new NameTextWatcher());
        calendarView.setOnDateChangeListener(new OnDateChangeListener());



        AppDiProvider provider = ((AppDiProvider) getApplication());

        taskRepository = provider.provideTaskRepository();
        CategoryRepository categoryRepository = provider.provideCategoryRepository();

        categories = categoryRepository.getAll();
        List<String> categoriesNames = getCategoriesNames();
        if (categories.size() == 0){
            Category category = new Category(1,"default category", R.color.green);
            categoryRepository.add(category);

            categories = categoryRepository.getAll();
            categoriesNames = getCategoriesNames();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner, categoriesNames);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        categoriesSpinner.setAdapter(adapter);

        presenter = new AddTaskPresenter(this);
        presenter.onCreateView();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable(TASK_TAG, task);
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void finishView() {
        finish();
    }

    @Override
    public void saveTask() {
        taskRepository.add(task);
    }

    @Override
    public void setNameField(String nameField) {
        nameEditText.setText(nameField);
    }

    @Override
    public void setDateField(long date) {
        calendarView.setDate(date);
    }

    @Override
    public void setSelectedCategory(int position) {
        categoriesSpinner.setSelection(position);
    }

    @Override
    public void setTaskName(String name) {
        task.setName(name);
    }

    @Override
    public void setTaskDate(String date) {
        task.setDate(date);
    }

    @Override
    public void setTaskCategory(Category category) {
        task.setCategory(category);
    }

    @Override
    public String getSelectedCategoryName() {
        return categoriesSpinner.getSelectedItem().toString();
    }

    @Override
    public List<Category> getCategories() {
        return categories;
    }

    @Override
    public Task getTask() {
        return task;
    }

    @Override
    public void onEmptyNameError() {
        new AddTaskActivity.EmptyNameDialogFragment().show(getSupportFragmentManager(), "EmptyName");
    }


    public static class EmptyNameDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final String TITLE = "Can`t create task";
            final String MESSAGE = "Fill name field!";

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialog);
            builder.setTitle(TITLE);
            builder.setMessage(MESSAGE);
            builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });

            return builder.create();
        }
    }


    private List<String> getCategoriesNames() {
        List<String> names = new ArrayList<>();
        for(Category category: categories){
            names.add(category.getName());
        }
        return names;
    }


    private class OnDateChangeListener implements CalendarView.OnDateChangeListener{
        @Override
        public void onSelectedDayChange(CalendarView view, int year, int month, int day) {
            presenter.onDateChange(year, month, day);
        }
    }

    private class NameTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            presenter.onNameChange(s.toString());
        }
    }

    private class OnClickCloseButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            presenter.onCloseButtonClicked();
        }
    }

    private class OnClickAddButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            presenter.onAddButtonClicked();
        }

    }

}
