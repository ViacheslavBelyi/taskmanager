package a.incorporated.slavik.taskmanager.ui.edit_screen;


import android.util.Log;

import java.util.Date;
import java.util.List;

import a.incorporated.slavik.taskmanager.domain.DateConverter;
import a.incorporated.slavik.taskmanager.domain.contract.edit_screen.AddTaskContract;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Task;

public class AddTaskPresenter implements AddTaskContract.Presenter {

    private final AddTaskContract.View view;

    AddTaskPresenter(AddTaskContract.View view){
        this.view = view;
    }

    @Override
    public void onCreateView() {

        Task task = view.getTask();

        String name = task.getName();
        view.setNameField(name);

        int position = getCategoryPosition(task.getCategory());

        Log.d("qaz", task.getCategory().toString());

        view.setSelectedCategory(position);

        if (task.getDate() != null){
            Date date = DateConverter.convertDate(task.getDate());
            if (date != null) {
                view.setDateField(date.getTime());
            }
        }

    }

    @Override
    public void onAddButtonClicked() {

        String taskName = view.getTask().getName();

        if (taskName == null || taskName.length() == 0){
            view.onEmptyNameError();

        } else {
            view.setTaskCategory(getSelectedCategory());
            view.saveTask();
            view.finishView();
        }
    }

    @Override
    public void onCloseButtonClicked() {
        view.finishView();
    }

    @Override
    public void onNameChange(String name) {
        view.setTaskName(name);
    }

    @Override
    public void onDateChange(int year, int month, int day) {
        Date date = new Date(year - 1900, month, day);
        String dateText = DateConverter.convertDate(date.getTime());
        view.setTaskDate(dateText);
    }


    private Category getSelectedCategory(){

        Category selectedCategory = null;

        String categoryName = view.getSelectedCategoryName();
        List<Category> categories = view.getCategories();

        for (Category category : categories){
            if (category.getName().equals(categoryName)){
                selectedCategory = category;
                break;
            }
        }
        return selectedCategory;
    }


    private int getCategoryPosition(Category category){

        List<Category> categories = view.getCategories();

        int position;

        for (position = 0; position < categories.size(); position++){
            if (category.equals(categories.get(position))){
                break;
            }
        }

        if (position >= categories.size()){
            position = 0;
        }

        return position;
    }

}
