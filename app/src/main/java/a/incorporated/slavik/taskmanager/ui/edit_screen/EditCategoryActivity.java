package a.incorporated.slavik.taskmanager.ui.edit_screen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.List;

import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;
import a.incorporated.slavik.taskmanager.domain.contract.edit_screen.EditCategoryContract;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Task;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EditCategoryActivity extends AppCompatActivity implements EditCategoryContract.View {

    public static final String CATEGORY_TAG = "a.incorporated.slavik.taskmanager.EditCategoryActivity.CATEGORY_TAG";

    @BindView(R.id.deleteButton)
    ImageButton deleteButton;
    @BindView(R.id.updateFab)
    FloatingActionButton updateButton;

    @BindView(R.id.nameEditText)
    EditText nameEditText;
    @BindView(R.id.categorySpinner)
    Spinner spinner;

    private Category category;

    private EditCategoryContract.Presenter presenter;


    private CategoryRepository repository;
    private TaskRepository taskRepository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_category_activity);

        ButterKnife.bind(this);

        category = (Category) getIntent().getExtras().get(CATEGORY_TAG);

        updateButton.setOnClickListener(new OnClickUpdateButtonListener());
        deleteButton.setOnClickListener(new OnClickDeleteButtonListener());

        nameEditText.addTextChangedListener(new NameTextWatcher());
        spinner.setOnItemSelectedListener(new OnItemSelectedListener());

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.categories_colors, R.layout.spinner);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(adapter);


        AppDiProvider provider = ((AppDiProvider) getApplication());
        repository = provider.provideCategoryRepository();
        taskRepository = provider.provideTaskRepository();

        presenter = new EditCategoryPresenter(this);
        presenter.onCreateView();

    }

    @Override
    public void setNameField(String nameField) {
        nameEditText.setText(nameField);
    }

    @Override
    public void setSelectedColorField(int position) {
        spinner.setSelection(position);
    }

    @Override
    public String getSelectedColorName() {
        return spinner.getSelectedItem().toString();
    }

    @Override
    public void updateCategory() {
        repository.update(category);
    }

    @Override
    public void deleteCategory() {
        repository.delete(category);
    }

    @Override
    public List<Task> getTasks() {
        return taskRepository.getAll();
    }


    @Override
    public void finishView() {
        finish();
    }

    @Override
    public void saveCategory() {
        // useless
    }

    @Override
    public void setCategoryName(String name) {
        category.setName(name);
    }

    @Override
    public void setCategoryColor(int color) {
        category.setColor(color);
    }

    @Override
    public int[] getColors() {
        return AddCategoryActivity.COLORS;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public void onEmptyNameError() {
        new AddCategoryActivity.EmptyNameDialogFragment().show(getSupportFragmentManager(), "EmptyName");  // todo needless tag?
    }

    @Override
    public void onRemoveError() {
        new EditCategoryActivity.CategoryDeleteErrorDialogFragment().show(getSupportFragmentManager(), "EmptyName");  // todo needless tag?
    }



    public static class CategoryDeleteErrorDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final String TITLE = "Can`t remove";
            final String MESSAGE = "Category is used !";

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialog);
            builder.setTitle(TITLE);
            builder.setMessage(MESSAGE);
            builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });

            return builder.create();
        }
    }


    private class OnClickDeleteButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            presenter.onDeleteButtonClicked();
        }
    }

    private class OnClickUpdateButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            presenter.onUpdateButtonClicked();
        }

    }


    private class NameTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            presenter.onNameChange(s.toString());
        }
    }

    private class OnItemSelectedListener implements Spinner.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            presenter.onColorChange(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
