package a.incorporated.slavik.taskmanager.ui.edit_screen;

import java.util.List;

import a.incorporated.slavik.taskmanager.domain.contract.edit_screen.EditCategoryContract;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Task;

public class EditCategoryPresenter extends AddCategoryPresenter implements EditCategoryContract.Presenter {

    private EditCategoryContract.View view;

    EditCategoryPresenter(EditCategoryContract.View view){
        super(view);
        this.view = view;
    }

    @Override
    public void onCreateView() {
        Category category = view.getCategory();

        view.setNameField(category.getName());

        int position = getSpinnerPosition(category.getColor());
        view.setSelectedColorField(position);
    }

    @Override
    public void onUpdateButtonClicked() {
        String categoryName = view.getCategory().getName();

        if (categoryName  == null || categoryName .length() == 0){
            view.onEmptyNameError();

        } else {
            view.updateCategory();
            view.finishView();
        }
    }

    @Override
    public void onDeleteButtonClicked() {

        if (isCategoryUse()) {
            view.onRemoveError();

        } else {
            view.deleteCategory();
            view.finishView();
        }
    }



    private int getSpinnerPosition(int color) {
        int position = 0;
        int[] colors = view.getColors();

        for (; position < colors.length; position++) {
            if (color == colors[position]) {
                break;
            }
        }

        if (position >= colors.length) {
            position = 0;
        }

        return position;
    }

    private boolean isCategoryUse(){

        Category category = view.getCategory();
        List<Task> tasks =  view.getTasks();

        boolean itUse = false;

        for (Task task : tasks){
            if (task.getCategory().equals(category)){
                itUse = true;
                break;
            }
        }

        return itUse;
    }
}
