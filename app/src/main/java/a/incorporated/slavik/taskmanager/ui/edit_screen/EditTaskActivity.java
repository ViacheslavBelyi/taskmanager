package a.incorporated.slavik.taskmanager.ui.edit_screen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;
import a.incorporated.slavik.taskmanager.domain.contract.edit_screen.EditTaskContract;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Task;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EditTaskActivity extends AppCompatActivity implements EditTaskContract.View {

    public static final String TASK_TAG = "a.incorporated.slavik.taskmanager.EditTaskActivity.TASK_TAG";

    @BindView(R.id.deleteButton)
    ImageButton deleteButton;
    @BindView(R.id.updateFab)
    FloatingActionButton updateButton;

    @BindView(R.id.nameEditText)
    EditText nameEditText;
    @BindView(R.id.calendarView)
    CalendarView calendarView;
    @BindView(R.id.categoriesSpinner)
    Spinner spinner;

    private Task task;

    private EditTaskContract.Presenter presenter;

    private List<Category> categories;

    private TaskRepository taskRepository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_task_activity);

        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            task = (Task) getIntent().getExtras().get(TASK_TAG);
        } else {
            task = savedInstanceState.getParcelable(TASK_TAG);
        }

        Log.d("qaz", "editTAct " + task.getCategory());

        nameEditText.addTextChangedListener(new NameTextWatcher());
        calendarView.setOnDateChangeListener(new OnDateChangeListener());

        deleteButton.setOnClickListener(new OnClickDeleteButtonListener());
        updateButton.setOnClickListener(new OnClickUpdateButtonListener());

        spinner.setOnItemSelectedListener(new OnCategorySelectedListener());


        AppDiProvider provider = ((AppDiProvider) getApplication());
        taskRepository = provider.provideTaskRepository();

        CategoryRepository categoryRepository = provider.provideCategoryRepository();
        categories = categoryRepository.getAll();

        List<String> categoriesNames = getCategoriesNames();

        if (categories.size() == 0){
            Category category = new Category(1,"default category", R.color.green);

            categoryRepository.add(category);

            categories = categoryRepository.getAll();
            categoriesNames = getCategoriesNames();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner, categoriesNames);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(adapter);

        presenter = new EditTaskPresenter(this);
        presenter.onCreateView();

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable(TASK_TAG, task);
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public Task getTask() {
        return task;
    }

    @Override
    public void onEmptyNameError() {
        new AddCategoryActivity.EmptyNameDialogFragment().show(getSupportFragmentManager(), "EmptyName");
    }


    @Override
    public void setNameField(String nameField) {
        nameEditText.setText(nameField);
    }

    @Override
    public void setDateField(long date) {
        calendarView.setDate(date);
    }

    @Override
    public void setSelectedCategory(int position) {
        spinner.setSelection(position);
    }

    @Override
    public void updateTask() {
        taskRepository.update(task);
    }

    @Override
    public void deleteTask() {
        taskRepository.delete(task);
    }

    @Override
    public void finishView() {
        finish();
    }

    @Override
    public void saveTask() {
        // needless
    }


    @Override
    public void setTaskName(String name) {
        task.setName(name);
    }

    @Override
    public void setTaskDate(String date) {
        task.setDate(date);
    }

    @Override
    public void setTaskCategory(Category category) {
        task.setCategory(category);
    }


    @Override
    public String getSelectedCategoryName() {
        return spinner.getSelectedItem().toString();
    }

    @Override
    public List<Category> getCategories() {
        return categories;
    }


    private List<String> getCategoriesNames() {

        List<String> names= new ArrayList<>();

        for(Category category: categories){
            names.add(category.getName());
        }
        return names;
    }


    private class OnDateChangeListener implements CalendarView.OnDateChangeListener{
        @Override
        public void onSelectedDayChange(CalendarView view, int year, int month, int day) {
            presenter.onDateChange(year, month, day);
        }
    }

    private class NameTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            presenter.onNameChange(s.toString());
        }
    }

    private class OnClickDeleteButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            presenter.onDeleteButtonClicked();
        }
    }

    private class OnClickUpdateButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            presenter.onUpdateButtonClicked();
        }

    }

    private class OnCategorySelectedListener implements Spinner.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            presenter.onCategorySelected(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

}
