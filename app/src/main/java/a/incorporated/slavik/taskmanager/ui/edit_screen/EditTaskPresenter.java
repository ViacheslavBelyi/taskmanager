package a.incorporated.slavik.taskmanager.ui.edit_screen;

import a.incorporated.slavik.taskmanager.domain.contract.edit_screen.EditTaskContract;
import a.incorporated.slavik.taskmanager.domain.entity.Category;

public class EditTaskPresenter extends AddTaskPresenter implements EditTaskContract.Presenter {

    private final EditTaskContract.View view;

    EditTaskPresenter(EditTaskContract.View view) {
        super(view);
        this.view = view;
    }


    @Override
    public void onUpdateButtonClicked() {
        String taskName = view.getTask().getName();

        if (taskName == null || taskName.length() == 0){
            view.onEmptyNameError();

        } else {
            view.updateTask();
            view.finishView();
        }
    }

    @Override
    public void onDeleteButtonClicked() {
        view.deleteTask();
        view.finishView();
    }

    @Override
    public void onCategorySelected(int position) {
        Category category = view.getCategories().get(position);
        view.setTaskCategory(category);
    }

}
