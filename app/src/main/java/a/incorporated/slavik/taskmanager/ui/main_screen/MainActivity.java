package a.incorporated.slavik.taskmanager.ui.main_screen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.animation.Animation;

import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;
import a.incorporated.slavik.taskmanager.di.layout.MainActivityDiProvider;
import a.incorporated.slavik.taskmanager.di.layout.MainActivityDiProviderImpl;
import a.incorporated.slavik.taskmanager.domain.contract.main_screen.MainContract;
import a.incorporated.slavik.taskmanager.domain.router.MainRouter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private MenuItem taskMenuItem;
    private MenuItem categoryMenuItem;
    private MenuItem settingsMenuItem;

    private MainActivityDiProvider provider;
    private MainRouter router;
    private MainContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.baseline_menu_black_36);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        taskMenuItem = navigationView.getMenu().findItem(R.id.task_menu_item);
        categoryMenuItem = navigationView.getMenu().findItem(R.id.category_menu_item);
        settingsMenuItem = navigationView.getMenu().findItem(R.id.settings_menu_item);

        navigationView.setNavigationItemSelectedListener(new ItemSelectedListener());

        provider = new MainActivityDiProviderImpl(this, (AppDiProvider)getApplication());
        router = provider.provideRouter();
        presenter = provider.providePresenter();

        //showTasks();
    }

    @Override
    public void showTasks() {
        onClickMenuItem(taskMenuItem);
        router.showTasks();
    }

    @Override
    public void showCategories() {
        onClickMenuItem(categoryMenuItem);
        router.showCategories();
    }

    @Override
    public void showSettings() {
        onClickMenuItem(settingsMenuItem);
        router.showSettings();
    }

    public MainActivityDiProvider getProvider(){
        return provider;
    }

    private class ItemSelectedListener implements NavigationView.OnNavigationItemSelectedListener {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            presenter.onMenuItemSelected(menuItem.getItemId());
            return true;
        }
    }

    private void onClickMenuItem(MenuItem menuItem){
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
