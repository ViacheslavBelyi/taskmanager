package a.incorporated.slavik.taskmanager.ui.main_screen;

import a.incorporated.slavik.taskmanager.domain.contract.main_screen.MainContract;

public class MainPresenter implements MainContract.Presenter {

    private static boolean isFirstStart = true;

    private static int TASK_ITEM_ID;
    private static int CATEGORY_ITEM_ID;
    private static int SETTINGS_ITEM_ID;

    private final MainContract.View view;

    public MainPresenter(MainContract.View view, int taskItemId, int categoryItemId, int settingsItemId){
        this.view = view;

        TASK_ITEM_ID = taskItemId;
        CATEGORY_ITEM_ID = categoryItemId;
        SETTINGS_ITEM_ID = settingsItemId;

        if (isFirstStart){
            isFirstStart = false;
            view.showTasks();
        }
    }

    @Override
    public void onMenuItemSelected(int itemId) {

        if (itemId == TASK_ITEM_ID){
            view.showTasks();
        }else if (itemId == CATEGORY_ITEM_ID){
            view.showCategories();
        }else if (itemId == SETTINGS_ITEM_ID){
            view.showSettings();
        }else {
            throw new ItemNoMentionException();
        }

    }

    class ItemNoMentionException extends RuntimeException{
    }


}
