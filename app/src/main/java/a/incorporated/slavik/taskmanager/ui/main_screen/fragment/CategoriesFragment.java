package a.incorporated.slavik.taskmanager.ui.main_screen.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.concurrent.ExecutionException;

import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.data.logic.dao.CategoryRepository;
import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;
import a.incorporated.slavik.taskmanager.di.fragment.CategoriesFragmentDiProvider;
import a.incorporated.slavik.taskmanager.di.fragment.CategoriesFragmentDiProviderImpl;
import a.incorporated.slavik.taskmanager.domain.contract.main_screen.ItemFragmentContract;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Item;
import a.incorporated.slavik.taskmanager.domain.router.ItemFragmentRouter;
import a.incorporated.slavik.taskmanager.data.implementation.async.CategoryReader;
import a.incorporated.slavik.taskmanager.ui.main_screen.router.CategoryFragmentRouterImpl;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesFragment extends Fragment implements ItemFragmentContract.View {

    @BindView(R.id.categories_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.categories_fragment_fab)
    FloatingActionButton fab;

    private CategoriesFragmentAdapter adapter;

    private ItemFragmentRouter router;

    private ItemFragmentContract.Presenter presenter;

    private CategoryRepository repository;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CategoriesFragmentDiProvider provider = new CategoriesFragmentDiProviderImpl((AppDiProvider) getActivity().getApplication());

        router = new CategoryFragmentRouterImpl(this.getContext(), provider);
        presenter = new CategoriesFragmentPresenter(this);

        repository = provider.provideRepository();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        ButterKnife.bind(this, view);

        fab.setOnClickListener(new FabListener());

        List<Category> categories = repository.getAll();

        adapter = new CategoriesFragmentAdapter(this, view.getContext(), categories);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        List<Category> categories = repository.getAll();

        adapter.setCategories(categories);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void startAddActivity() {
        router.startAddItemActivity();
    }

    @Override
    public void startEditActivity(Item item) {
        router.startEditItemActivity(item);
    }

    @Override
    public ItemFragmentContract.Presenter getPresenter() {
        return presenter;
    }

    private class FabListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            presenter.onAddButtonClicked();
        }
    }


}
