package a.incorporated.slavik.taskmanager.ui.main_screen.fragment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.domain.contract.main_screen.ItemFragmentContract;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesFragmentAdapter extends RecyclerView.Adapter<CategoriesFragmentAdapter.ViewHolder>
        implements ItemFragmentContract.ItemsView {

    private final LayoutInflater inflater;
    private List<Category> categories;

    private final ItemFragmentContract.View view ;

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    CategoriesFragmentAdapter(ItemFragmentContract.View view, Context context, List<Category> categories){
        this.inflater = LayoutInflater.from(context);
        this.categories = categories;

        this.view = view;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.category_item , parent, false);
        view.setOnClickListener(new ItemTapListener ());
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Category category = categories.get(position);

        viewHolder.nameField.setText(category.getName());
        viewHolder.colorField.setBackgroundResource(category.getColor());

        viewHolder.setTag(category);

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_text_view)
        TextView nameField;
        @BindView(R.id.color_text_view)
        TextView colorField;

        private final View holderView;

        ViewHolder(View v){
            super(v);
            ButterKnife.bind(this,v);

            this.holderView = v;
        }

        void setTag(Object object){
            holderView.setTag(object);
        }

    }

    private class ItemTapListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Category category = (Category)v.getTag();
            view.getPresenter().onItemTapped(category);
        }
    }


}
