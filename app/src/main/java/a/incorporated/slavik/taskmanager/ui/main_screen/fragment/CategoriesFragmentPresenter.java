package a.incorporated.slavik.taskmanager.ui.main_screen.fragment;

import a.incorporated.slavik.taskmanager.domain.contract.main_screen.ItemFragmentContract;
import a.incorporated.slavik.taskmanager.domain.entity.Item;

public class CategoriesFragmentPresenter implements ItemFragmentContract.Presenter {

    private final ItemFragmentContract.View view;

    CategoriesFragmentPresenter (ItemFragmentContract.View view){
        this.view = view;
    }

    @Override
    public void onAddButtonClicked() {
        view.startAddActivity();
    }

    @Override
    public void onItemTapped(Item item) {
        view.startEditActivity(item);
    }


}
