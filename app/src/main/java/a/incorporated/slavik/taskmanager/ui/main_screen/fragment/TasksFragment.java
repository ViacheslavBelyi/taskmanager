package a.incorporated.slavik.taskmanager.ui.main_screen.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.concurrent.ExecutionException;

import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.data.logic.dao.TaskRepository;
import a.incorporated.slavik.taskmanager.di.app.AppDiProvider;
import a.incorporated.slavik.taskmanager.di.fragment.TasksFragmentDiProvider;
import a.incorporated.slavik.taskmanager.di.fragment.TasksFragmentDiProviderImpl;
import a.incorporated.slavik.taskmanager.domain.contract.main_screen.ItemFragmentContract;
import a.incorporated.slavik.taskmanager.domain.entity.Item;
import a.incorporated.slavik.taskmanager.domain.entity.Task;
import a.incorporated.slavik.taskmanager.domain.router.ItemFragmentRouter;
import a.incorporated.slavik.taskmanager.data.implementation.async.TasksReader;
import a.incorporated.slavik.taskmanager.ui.main_screen.router.TaskFragmentRouterImpl;

public class TasksFragment extends Fragment implements ItemFragmentContract.View {

    private RecyclerView recyclerView;
    private TasksFragmentAdapter adapter;

    private ItemFragmentContract.Presenter presenter;
    private ItemFragmentRouter router;

    private TaskRepository repository;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TasksFragmentDiProvider provider = new TasksFragmentDiProviderImpl((AppDiProvider) getActivity().getApplication());

        presenter = new TasksFragmentPresenter(this);
        router = new TaskFragmentRouterImpl(this.getContext(), provider);

        repository = provider.provideRepository();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tasks, container, false);

        recyclerView =  view.findViewById(R.id.tasks_recycler_view);
        FloatingActionButton fab = view.findViewById(R.id.task_fragment_fab);
        fab.setOnClickListener(new FabListener());

        List<Task> tasks = repository.getAll();

        adapter = new TasksFragmentAdapter(this, view.getContext(), tasks);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        List<Task> tasks = repository.getAll();

        adapter.setTasks(tasks);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void startAddActivity() {
        router.startAddItemActivity();
    }

    @Override
    public void startEditActivity(Item item) {
        router.startEditItemActivity(item);
    }

    @Override
    public ItemFragmentContract.Presenter getPresenter() {
        return presenter;
    }


    private class FabListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            presenter.onAddButtonClicked();
        }
    }


}
