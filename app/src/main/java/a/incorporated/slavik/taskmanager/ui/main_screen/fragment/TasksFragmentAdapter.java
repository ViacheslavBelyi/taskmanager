package a.incorporated.slavik.taskmanager.ui.main_screen.fragment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.domain.contract.main_screen.ItemFragmentContract;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Task;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TasksFragmentAdapter extends RecyclerView.Adapter<TasksFragmentAdapter.ViewHolder>
        implements ItemFragmentContract.ItemsView {

    private final LayoutInflater inflater;
    private List<Task> tasks;

    private final ItemFragmentContract.View view ;

    void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    TasksFragmentAdapter(ItemFragmentContract.View view, Context context, List<Task> tasks){
        this.inflater = LayoutInflater.from(context);
        this.tasks = tasks;

        this.view = view;
    }

    @NonNull
    @Override
    public TasksFragmentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.task_item , parent, false);
        view.setOnClickListener(new ItemTapListener());
        return new TasksFragmentAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        final String NO_DATE_TEXT = "No deadline";

        Task task = tasks.get(position);

        Category category = task.getCategory();

        viewHolder.nameField.setText(task.getName());
        viewHolder.categoryField.setText(category.getName());
        viewHolder.categoryField.setBackgroundResource(category.getColor());

        String date = task.getDate();
        if (date == null || date.equals("null")){
            viewHolder.dateField.setText(NO_DATE_TEXT);
        } else {
            viewHolder.dateField.setText(date);
        }

        viewHolder.setTag(task);

    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name_text_view)
        TextView nameField;
        @BindView(R.id.date_text_view)
        TextView dateField;
        @BindView(R.id.category_text_view)
        TextView categoryField;

        private final View holderView;

        ViewHolder(View v){
            super(v);
            ButterKnife.bind(this, v);

            this.holderView = v;
        }
        void setTag(Object object){
            holderView.setTag(object);
        }

    }

    private class ItemTapListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Task task = (Task)v.getTag();
            view.getPresenter().onItemTapped(task);
        }
    }
}

