package a.incorporated.slavik.taskmanager.ui.main_screen.router;

import android.content.Context;
import android.content.Intent;

import a.incorporated.slavik.taskmanager.di.fragment.CategoriesFragmentDiProvider;
import a.incorporated.slavik.taskmanager.domain.entity.Category;
import a.incorporated.slavik.taskmanager.domain.entity.Item;
import a.incorporated.slavik.taskmanager.domain.router.ItemFragmentRouter;
import a.incorporated.slavik.taskmanager.ui.edit_screen.AddCategoryActivity;
import a.incorporated.slavik.taskmanager.ui.edit_screen.EditCategoryActivity;

public class CategoryFragmentRouterImpl implements ItemFragmentRouter {

    private final Context context;

    public CategoryFragmentRouterImpl(Context context, CategoriesFragmentDiProvider provider){
        this.context = context;
        CategoriesFragmentDiProvider provider1 = provider;
    }


    @Override
    public void startAddItemActivity() {
        Intent intent = new Intent(context, AddCategoryActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void startEditItemActivity(Item item) {
        Intent intent = new Intent(context, EditCategoryActivity.class);
        intent.putExtra(EditCategoryActivity.CATEGORY_TAG, (Category) item);
        context.startActivity(intent);
    }



}
