package a.incorporated.slavik.taskmanager.ui.main_screen.router;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import a.incorporated.slavik.taskmanager.R;
import a.incorporated.slavik.taskmanager.di.layout.MainActivityDiProvider;
import a.incorporated.slavik.taskmanager.domain.router.MainRouter;
import a.incorporated.slavik.taskmanager.ui.main_screen.MainActivity;
import a.incorporated.slavik.taskmanager.ui.main_screen.fragment.CategoriesFragment;
import a.incorporated.slavik.taskmanager.ui.main_screen.fragment.SettingsFragment;
import a.incorporated.slavik.taskmanager.ui.main_screen.fragment.TasksFragment;

public class MainRouterImpl implements MainRouter {
    private static final int CONTAINER_ID = R.id.fragment_container;

    private final FragmentManager fragmentManager;
    private final MainActivityDiProvider mainActivityDiProvider;

    public MainRouterImpl(MainActivity mainActivity){
        MainActivity mainActivity1 = mainActivity;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
        this.mainActivityDiProvider = mainActivity.getProvider();
    }

    @Override
    public void showTasks() {
        TasksFragment fragment = mainActivityDiProvider.provideTasksFragment();
        replaceFragment(fragment);
    }

    @Override
    public void showCategories() {
        CategoriesFragment fragment = mainActivityDiProvider.provideCategoryFragment();
        replaceFragment(fragment);
    }

    @Override
    public void showSettings() {
        replaceFragment(new SettingsFragment());
    }

    private void replaceFragment(Fragment fragment){
        fragmentManager.beginTransaction()
                .replace(MainRouterImpl.CONTAINER_ID, fragment)
                .commit();
    }


}
