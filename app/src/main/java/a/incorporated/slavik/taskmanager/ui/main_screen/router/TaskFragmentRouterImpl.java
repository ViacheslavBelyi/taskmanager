package a.incorporated.slavik.taskmanager.ui.main_screen.router;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import a.incorporated.slavik.taskmanager.di.fragment.TasksFragmentDiProvider;
import a.incorporated.slavik.taskmanager.domain.entity.Item;
import a.incorporated.slavik.taskmanager.domain.entity.Task;
import a.incorporated.slavik.taskmanager.domain.router.ItemFragmentRouter;
import a.incorporated.slavik.taskmanager.ui.edit_screen.AddTaskActivity;
import a.incorporated.slavik.taskmanager.ui.edit_screen.EditTaskActivity;

public class TaskFragmentRouterImpl implements ItemFragmentRouter {

    private final Context context;

    public TaskFragmentRouterImpl(Context context, TasksFragmentDiProvider provider){
        this.context = context;
        TasksFragmentDiProvider provider1 = provider;
    }

    @Override
    public void startAddItemActivity() {
        Intent intent = new Intent(context, AddTaskActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void startEditItemActivity(Item item) {
        Intent intent = new Intent(context, EditTaskActivity.class);
        intent.putExtra(EditTaskActivity.TASK_TAG, (Task) item);
        context.startActivity(intent);

    }

}
